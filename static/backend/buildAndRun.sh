#!/bin/sh

mvn -f /opt/backend/ clean package -DskipTests
java -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 /opt/backend/target/backend*.jar
